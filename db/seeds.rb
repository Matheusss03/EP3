# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = CreateAdminService.new.call
puts 'CREATED ADMIN USER: ' << user.email
Product.delete_all
Product.create! id: 1, name: "Morango Maki", price: 0.89, active: true
Product.create! id: 2, name: "Hot Philadelphia", price: 1.50, active: true
Product.create! id: 3, name: "Hossomaki (Sushi tradicional)", price: 0.69, active: true
Product.create! id: 4, name: "Hossomaki Kani (Carne de carangueijo)", price: 0.89, active: true
Product.create! id: 5, name: "Uramaki Ebitem (Camarão empanado com cream cheese)", price: 1.69, active: true
Product.create! id: 6, name: "Barca - 60 itens", price: 49.90, active: true
Product.create! id: 7, name: "Barca - 30 itens", price: 29.90, active: true
Product.create! id: 8, name: "Kombinado - 16 itens", price: 19.90, active: true
Product.create! id: 9, name: "Sashimi de salmão", price: 3.40, active: true
Product.create! id: 10, name: "Sashimi de polvo", price: 4.90, active: true
Product.create! id: 11, name: "Temaki de salmão e cream cheese", price: 19.90, active: true
Product.create! id: 12, name: "Temaki Filadélfia", price: 19.90, active: true
Product.create! id: 13, name: "Nigiri de Salmão", price: 4.90, active: true
Product.create! id: 14, name: "Yakisoba Clássico", price: 14.90, active: true
Product.create! id: 15, name: "Yakisoba de Camarão", price: 19.90, active: true

OrderStatus.delete_all
OrderStatus.create! id: 1, name: "In Progress"
OrderStatus.create! id: 2, name: "Placed"
OrderStatus.create! id: 3, name: "Shipped"
OrderStatus.create! id: 4, name: "Cancelled"